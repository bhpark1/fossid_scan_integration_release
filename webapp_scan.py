# SPDX-License-Identifier: MIT
# Author: Michal Brutvan
# Copyright: FossID AB 2020

# usage: webapp_scan.py [-h] git_url branch

# Run FossID WebApp Scan

# positional arguments:
#   git_url     URL of the git repository
#   branch      a branch to scan

# optional arguments:
#   -h, --help        show this help message and exit
#   --ssh:            force git connection over ssh even if the git url is with http(s).
#   --unsafe-wait:    Assume the scan finished when the progress reaches 100 percent. Work-around for bug GUI-2466.

import requests
import json
import time
import datetime
import os
import argparse
import re
from dotenv import load_dotenv

SCAN_TIMEOUT_MINUTES = 300


class WebappScan:
    def __init__(self, git_url: str, branch: str, unsafe_wait: bool):
        self.host = WebappScan._get_env_var_or_raise("FOSSID_HOST")
        self.token = WebappScan._get_env_var_or_raise("FOSSID_TOKEN")
        self.username = WebappScan._get_env_var_or_raise("FOSSID_USER")
        self.scan_code = WebappScan._get_scan_code_from_git_url(
            git_url, branch)
        self.project_code = WebappScan._get_project_code_from_git_url(git_url)
        self.git_url = git_url
        self.branch = branch
        self.reference_scan_id = None
        self.unsafe_wait = unsafe_wait
        self.scan_id = None
        if "REFERENCE_SCAN_ID" in os.environ.keys():
            self.reference_scan_id = os.environ["REFERENCE_SCAN_ID"]

    @staticmethod
    def _get_scan_code_from_git_url(url: str, branch: str):
        parts = url.replace(".git", "").split("/")
        return "{}-{}".format(parts[len(parts) - 1], branch)

    @staticmethod
    def _get_project_code_from_git_url(url: str):
        parts = url.replace(".git", "").split("/")
        return "{}".format(parts[len(parts) - 1])

    @staticmethod
    def _get_env_var_or_raise(var_name: str):
        if var_name not in os.environ.keys() or os.environ[var_name] == "":
            raise Exception("{} is not set".format(var_name))
        return os.environ[var_name]

    def _send_request(self, payload: dict) -> dict:
        url = "{}/webapp/api.php".format(self.host)
        headers = {
            "Accept": "*/*",
            "Accept-Language": "en-US,en;q=0.5",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "Origin": "http://localhost:2880",
            "Connection": "keep-alive",
        }
        response = requests.request(
            "POST", url, headers=headers, data=json.dumps(payload)
        )
        return json.loads(response.text)

    def _delete_existing_scan(self):
        payload = {
            "group": "scans",
            "action": "delete",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
                "delete_identifications": "true",
            },
        }
        return self._send_request(payload)

    def _call_create_scan(self) -> dict:
        payload = {
            "group": "scans",
            "action": "create",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
                "scan_name": self.scan_code,
                "git_repo_url": self.git_url,
                "git_branch": self.branch,
                "project_code": self.project_code
            },
        }
        return self._send_request(payload)

    def _create_webapp_scan(self) -> bool:
        response = self._call_create_scan()
        if response["status"] != "1":
            raise Exception(
                "Failed to create scan {}: {}".format(self.scan_code, response)
            )
        if "error" in response.keys():
            raise Exception(
                "Failed to create scan {}: {}".format(
                    self.scan_code, response["error"])
            )
        return response["data"]["scan_id"]

    def _download_content_from_git(self):
        payload = {
            "group": "scans",
            "action": "download_content_from_git",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }
        response = self._send_request(payload)
        if response["status"] != "1":
            message = "Failed to get content from git for scan {}: {}".format(
                self.scan_code, response["error"]
            )
            raise Exception(message)

    def _get_git_download_status(self):
        payload = {
            "group": "scans",
            "action": "check_status_download_content_from_git",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }
        return self._send_request(payload)

    def _get_scan_status(self, type: str):
        payload = {
            "group": "scans",
            "action": "check_status",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
                "project_code": "test",
                "type": type,
            },
        }
        response = self._send_request(payload)
        if response["status"] != "1":
            raise Exception(
                "Failed to retrieve scan status from \
                scan {}: {}".format(
                    self.scan_code, response["error"]
                )
            )
        return response["data"]

    def _get_start_scan_default_payload(self):
        return {
            "group": "scans",
            "action": "run",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }

    def _get_start_scan_payload_with_reuse(self):
        return {
            "group": "scans",
            "action": "run",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
                "reuse_identification": "1",
                "identification_reuse_type": "specific_project",
                "specific_code": self.project_code
            },
        }

    def _start_scan(self):
        payload = self._get_start_scan_payload_with_reuse()
        response = self._send_request(payload)
        if response["status"] != "1":
            raise Exception(
                "Failed to start scan {}: {}".format(
                    self.scan_code, response["error"])
            )

    def _start_dependency_analysis(self):
        payload = {
            "group": "scans",
            "action": "run_dependency_analysis",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }
        response = self._send_request(payload)
        if response["status"] != "1":
            raise Exception(
                "Failed to start dependency analysis scan {}: {}".format(
                    self.scan_code, response["error"]
                )
            )

    def _wait_for_git_download(self):
        print("Downloading {}".format(self.scan_code))

        timeout_minutes = SCAN_TIMEOUT_MINUTES
        timeout_at = datetime.datetime.now() + datetime.timedelta(
            minutes=timeout_minutes
        )
        git_downloaded = False
        while not git_downloaded:
            response = self._get_git_download_status()

            if response["data"] == "FINISHED":
                git_downloaded = True

            if "fatal" in response.keys():
                raise Exception(
                    "Failed to retrieve git pull status from \
                    scan {}: {}".format(
                        self.scan_code, response["fatal"]
                    )
                )

            if "error" in response.keys():
                raise Exception(
                    "Failed to retrieve git pull status from \
                    scan {}: {}".format(
                        self.scan_code, response["error"]
                    )
                )

            time.sleep(5)
            if datetime.datetime.now() > timeout_at:
                print("git download timeout {}".format(self.scan_code))
                raise Exception("git download timeout")
            if "message" in response.keys() and "Git download failed" in response["message"]:
                raise Exception(response["message"])

    def _wait_for_scan_to_finish(self, scan_type: str):
        is_finished = False
        timeout_at = datetime.datetime.now() + datetime.timedelta(
            minutes=SCAN_TIMEOUT_MINUTES
        )

        while not is_finished:
            scan_status = self._get_scan_status(scan_type)

            if scan_status["is_finished"] == "1":
                is_finished = True
            if self.unsafe_wait and scan_status["percentage_done"] == "100%":
                print("-u flag is on, assuming the scan has finished.")
                is_finished = True
            if scan_status["status"] == "KILLED":
                raise Exception("Scan failed, status: KILLED")
            time.sleep(5)
            print(
                "Scan {} is running {}: {}".format(
                    self.scan_code, scan_type, scan_status["percentage_done"]
                )
            )
            if datetime.datetime.now() > timeout_at:
                print("{} timeout: {}".format(scan_type, self.scan_code))
                raise Exception("scan timeout")
        return True

    def _get_pending_files(self):
        payload = {
            "group": "scans",
            "action": "get_pending_files",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }
        response = self._send_request(payload)
        if response["status"] == "1" and "data" in response.keys():
            return response["data"]
        else:
            raise Exception(
                "Error getting dependency analysis \
                result: {}".format(
                    response
                )
            )

    def _get_dependency_analysis_result(self):
        payload = {
            "group": "scans",
            "action": "get_dependency_analysis_results",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code,
            },
        }
        response = self._send_request(payload)
        if response["status"] == "1" and "data" in response.keys():
            return response["data"]
        else:
            raise Exception(
                "Error getting dependency analysis \
                result: {}".format(
                    response
                )
            )

    def _cancel_scan(self):
        payload = {
            "group": "scans",
            "action": "cancel_run",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": self.scan_code
            },
        }
        response = self._send_request(payload)
        if response["status"] != "1":
            raise Exception("Error cancelling scan: {}".format(response))

    def _assert_scan_can_start(self):
        scan_status = self._get_scan_status("SCAN")
        if scan_status["status"] not in ["FINISHED", "INTERRUPTED"]:
            print("Current status of the scan is {}. Cancelling...".format(
                scan_status["status"]))
            self._cancel_scan()

    def _check_if_scan_exists(self, scan_code=None):
        scan_code_to_check = scan_code
        if scan_code_to_check is None:
            scan_code_to_check = self.scan_code
        payload = {
            "group": "scans",
            "action": "get_information",
            "data": {
                "username": self.username,
                "key": self.token,
                "scan_code": scan_code_to_check,
            },
        }
        response = self._send_request(payload)
        if (
            "status" in response.keys()
            and response["status"] == "1"
            and "data" in response.keys()
            and "code" in response["data"].keys()
            and response["data"]["code"] == scan_code_to_check
        ):
            return True, response["data"]["id"]

        if "error" in response.keys() and \
            ("Scan does not exist" in response["error"] or 
             "Scan not found" in response["error"] or 
             "Row %id% in table %tablename% not found" in response["error"]):
            return False, None
        elif "error" in response.keys():
            raise Exception("Failed to check scan status: {}".format(response))

        raise Exception(
            "Unexpected error when checking scan status. Response: {}".format(response))

    def _check_if_project_exists(self):
        payload = {
            "group": "projects",
            "action": "get_all_scans",
            "data": {
                "username": self.username,
                "key": self.token,
                "project_code": self.project_code
            }
        }
        response = self._send_request(payload)
        if response["status"] == "0" and "Project does not exist" == response["error"]:
            return False
        if response["status"] == "0":
            raise Exception(
                "Failed to get project status: {}".format(response))
        return True

    def _create_project(self):
        payload = {
            "group": "projects",
            "action": "create",
            "data": {
                "username": self.username,
                "key": self.token,
                "project_code": self.project_code,
                "project_name": self.project_code,
                "description": "CI/CD Project for {}".format(self.git_url),
            }
        }
        response = self._send_request(payload)
        if response["status"] != "1":
            raise Exception("Failed to create project: {}".format(response))
        print("Created project {}".format(self.project_code))

    def _assert_project_exists(self):
        if not self._check_if_project_exists():
            self._create_project()

    def run_scan(self):
        self._assert_project_exists()

        scan_exists, scan_id = self._check_if_scan_exists()
        if not scan_exists:
            scan_id = self._create_webapp_scan()
        else:
            self._assert_scan_can_start()
        self.scan_id = scan_id
        self._download_content_from_git()
        self._wait_for_git_download()
        print("starting scan {}".format(self.scan_code))
        self._start_scan()
        self._wait_for_scan_to_finish("SCAN")
        pending_files = self._get_pending_files()
        return len(pending_files)

    def get_scan_url(self):
        return "{}/webapp/index.php?form=scan&sid={}".format(self.host, self.scan_id)


def to_ssh(http_url: str):
    import re
    p1 = re.compile('//[a-zA-Z0-9_-]+:[a-zA-Z0-9_-]+@')
    s = p1.sub('//', http_url)
    p2 = re.compile('(http://|https://)')
    s = p2.sub('git@', s)
    p3 = re.compile('/')
    s = p3.sub(':', s, 1)
    return s


def parse_cmdline_args():
    parser = argparse.ArgumentParser(description='Run FossID WebApp Scan')
    parser.add_argument(
        "git_url", help="URL of the git repository")
    parser.add_argument("branch", help="A branch to scan")
    parser.add_argument("--ssh",  action="store_true",
                        help="force git connection over ssh even if the git url is with https.", dest="ssh")
    parser.add_argument("--unsafe-wait", action="store_true",
                        help="Assume the scan finished when the progress reaches 100 percent. Work-around for bug GUI-2466.", dest="unsafe_wait")
    args = parser.parse_args()
    return args.git_url, args.branch, args.ssh, args.unsafe_wait


def fix_git_url(git_url: str, force_use_ssh: bool):
    if not force_use_ssh:
        return git_url
    fixed_git_url = git_url
    if git_url.startswith("http://") or git_url.startswith("https://"):
        fixed_git_url = to_ssh(git_url)
    print("fixed git url: {}".format(fixed_git_url))
    return fixed_git_url


def main():
    load_dotenv()
    git_url, branch, force_use_ssh, unsafe_wait = parse_cmdline_args()
    git_url = fix_git_url(git_url, force_use_ssh)
    scan = WebappScan(git_url, branch, unsafe_wait)
    try:
        count_pending_files = scan.run_scan()
        print("files pending identification: {}".format(count_pending_files))
        print("See the scan results at {}".format(scan.get_scan_url()))
        exit(count_pending_files)
    except Exception as ex:
        print(ex)
        exit(1)


main()
